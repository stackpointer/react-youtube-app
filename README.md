# React YouTube App

A simple YouTube search app using [React](https://facebook.github.io/react/)

## Introduction

YouTube app written in React that uses YouTube v3 Data API to fetch and display results.

![preview](screenshot/screenshot01.jpg)

## Installation

1. Checkout repo and  install dependencies
```
$ git clone https://gitlab.com/stackpointer/react-youtube-app.git
$ cd react-youtube-app
$ npm install
$ npm start
```

2. Copy `src/config-dist.json` to `src/config.json`

3. Edit the `src/config.json` file and replace the string `ENTER-YOUTUBE-API-KEY-HERE` with your own YouTube API v3 key. You can get the API key for free from Google Developers Console. Refer to Google's [Getting Started](https://developers.google.com/youtube/v3/getting-started) guide for more info.
 
4. Start the application
```
$ npm start
```

5. Launch browser at `http://localhost:8080`

## Features

- Search bar input uses a debounce time of 500ms
- Display first 5 search results
- Select video to be played from search results

## Copyright and License
Copyright (c) 2017, Mohamed Ibrahim. Code released under the [MIT](LICENSE) license.
